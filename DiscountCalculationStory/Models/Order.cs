namespace DiscountCalculationStory.Models
{
    public class Order
    {
        public decimal OrderPrice { get; set; }

        public decimal TotalTax { get; set; }

        public decimal GrossOrderPrice => this.OrderPrice + this.TotalTax;
    }
}
