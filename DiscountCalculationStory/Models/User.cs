namespace DiscountCalculationStory.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class User
    {
        public IList<Order> Orders { get; set; }

        public decimal CalculateDiscount()
        {
            var totalPriceForAllOrders = CalculateTotalPriceOfAllOrders(this);
            var discount = DiscountToThresholdPercentage
                .LastOrDefault(entry => totalPriceForAllOrders >= entry.Key).Value;
            return Convert.ToDecimal(discount);
        }

        private const double FIVE_PERCENT_DISCOUNT = 0.05d;
        private const double TEN_PERCENT_DISCOUNT = 0.1d;
        private const double FIFTEEN_PERCENT_DISCOUNT = 0.15d;
        private const int DISCOUNT_LEVEL1_THRESHOLD_USD = 1000;
        private const int DISCOUNT_LEVEL2_THRESHOLD_USD = 3000;
        private const int DISCOUNT_LEVEL3_THRESHOLD_USD = 5000;
        private static readonly Dictionary<int, double> DiscountToThresholdPercentage = new()
        {
            [DISCOUNT_LEVEL1_THRESHOLD_USD] = FIVE_PERCENT_DISCOUNT,
            [DISCOUNT_LEVEL2_THRESHOLD_USD] = TEN_PERCENT_DISCOUNT,
            [DISCOUNT_LEVEL3_THRESHOLD_USD] = FIFTEEN_PERCENT_DISCOUNT,
        };

        private static double CalculateTotalPriceOfAllOrders(User user) =>
            user.Orders
                .Select(o => o.GrossOrderPrice)
                .Sum(p => decimal.ToDouble(p));
    }
}
