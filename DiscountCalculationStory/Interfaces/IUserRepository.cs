namespace DiscountCalculationStory.Interfaces
{
    using DiscountCalculationStory.Models;

    public interface IUserRepository
    {
        User GetUserById(int userId);
    }
}
