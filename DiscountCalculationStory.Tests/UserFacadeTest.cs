namespace DiscountCalculationStory.Tests
{
    using System;
    using System.Collections.Generic;
    using DiscountCalculationStory.Interfaces;
    using DiscountCalculationStory.Models;
    using Moq;
    using Xunit;

    public class UserFacadeTest
    {
        private readonly User user;
        private readonly Order order;
        private readonly Mock<IUserRepository> userRepositoryMock;
        private readonly UserFacade userFacade;
        private const int PRECISION_TOLERANCE = 3;
        private const int USER_ID = 1;

        private const double ZERO_PERCENT_DISCOUNT = 0.0d;
        private const double FIVE_PERCENT_DISCOUNT = 0.05d;
        private const double TEN_PERCENT_DISCOUNT = 0.1d;
        private const double FIFTEEN_PERCENT_DISCOUNT = 0.15d;
        public UserFacadeTest()
        {
            this.user = new User();
            this.order = new Order();
            this.userRepositoryMock = new Mock<IUserRepository>();
            this.userFacade = new UserFacade(this.userRepositoryMock.Object);
        }

        [Theory]
        [InlineData(100, ZERO_PERCENT_DISCOUNT)]
        [InlineData(1001, FIVE_PERCENT_DISCOUNT)]
        [InlineData(3001, TEN_PERCENT_DISCOUNT)]
        [InlineData(5001, FIFTEEN_PERCENT_DISCOUNT)]
        public void GetDiscountForUserById_CorrectDiscountCalculated(
            decimal orderPrice,
            double expectedDiscount)
        {
            // Arrange
            this.order.OrderPrice = orderPrice;
            this.order.TotalTax = 100;
            this.user.Orders = new List<Order> { this.order };
            _ = this.userRepositoryMock
                .Setup(repository => repository.GetUserById(USER_ID))
                .Returns(this.user);

            // Act
            var discount = this.userFacade.GetDiscountForUserById(USER_ID);

            // Assert
            Assert.Equal(expectedDiscount, Convert.ToDouble(discount), PRECISION_TOLERANCE);
        }

        [Fact]
        public void GetDiscountForUserById_NetPriceIsLessThanThresholdButWithTaxTogetherIsMore_FivePercentDiscount()
        {
            // Arrange
            this.order.OrderPrice = 900;
            this.order.TotalTax = 100;
            this.user.Orders = new List<Order> { this.order };
            _ = this.userRepositoryMock
                .Setup(repository => repository.GetUserById(USER_ID))
                .Returns(this.user);

            // Act
            var discount = this.userFacade.GetDiscountForUserById(USER_ID);

            // Assert
            Assert.Equal(FIVE_PERCENT_DISCOUNT, Convert.ToDouble(discount), PRECISION_TOLERANCE);
        }

        [Fact]
        public void GetDiscountForUserById_UserHadMultipleOrders_TenPercentDiscount()
        {
            // Arrange
            this.order.OrderPrice = 900;
            this.order.TotalTax = 100;
            this.user.Orders = new List<Order> { this.order, this.order, this.order };
            _ = this.userRepositoryMock
                .Setup(repository => repository.GetUserById(USER_ID))
                .Returns(this.user);

            // Act
            var discount = this.userFacade.GetDiscountForUserById(USER_ID);

            // Assert
            Assert.Equal(TEN_PERCENT_DISCOUNT, Convert.ToDouble(discount), PRECISION_TOLERANCE);
        }

        [Fact]
        public void GetDiscountForUserById_AnyOrders_ZeroPercentDiscount()
        {
            // Arrange
            this.user.Orders = new List<Order>();
            _ = this.userRepositoryMock
                .Setup(repository => repository.GetUserById(USER_ID))
                .Returns(this.user);

            // Act
            var discount = this.userFacade.GetDiscountForUserById(USER_ID);

            // Assert
            Assert.Equal(ZERO_PERCENT_DISCOUNT, Convert.ToDouble(discount), PRECISION_TOLERANCE);
        }
    }
}
