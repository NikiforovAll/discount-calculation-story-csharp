namespace DiscountCalculationStory.Infrastructure.Data
{
    using DiscountCalculationStory.Infrastructure.Data.Entities;
    using Microsoft.EntityFrameworkCore;

    public class UserContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
            optionsBuilder.UseInMemoryDatabase("Database");
    }
}
