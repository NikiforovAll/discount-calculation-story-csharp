namespace DiscountCalculationStory.Infrastructure.Data.Entities
{
    using System;
    using System.Collections.Generic;

    public class Order
    {
        public int OrderId { get; set; }

        public decimal OrderPrice { get; set; }

        public decimal TotalTax { get; set; }

        public double Discount { get; set; }

        public int BuyerId { get; set; }

        public DateTimeOffset PurchaseDate { get; set; }

        public IList<Product> Products { get; set; }
    }
}
