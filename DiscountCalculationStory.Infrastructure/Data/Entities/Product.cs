namespace DiscountCalculationStory.Infrastructure.Data.Entities
{
    using System.Collections.Generic;

    public class Product
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public string CategoryName { get; set; }

        public double ProductRating { get; set; }

        public IList<string> Reviews { get; set; }

    }
}
