namespace DiscountCalculationStory.Infrastructure.Data.Entities
{
    using System;
    using System.Collections.Generic;

    public class User
    {
        public int UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTimeOffset SignUpDate { get; set; }

        public DateTimeOffset LastLoginDate { get; set; }

        public int ReferrerId { get; set; }

        public string PartnerCode { get; set; }

        public IList<Order> Orders { get; set; }
    }
}
